<!DOCTYPE html>
<html lang="pt-BR">
<head>
  <title>KF</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="_css/bootstrap.min.css">  
  <script src="_js/jquery.min.js"></script>
  <link href="_css/estilo.css" rel="stylesheet">
  <script src="_js/bootstrap.min.js"></script>
</head>
<body>
<div class="login-form">
    <form action="" method="post">
		<div class="avatar">
			<img src="_imagens/iconSemBack.png" alt="Avatar">
		</div>
        <h2 class="text-center">Acessar</h2>   
        <div class="form-group">
        	<input type="text" class="form-control" name="username" placeholder="Email" required="required">
        </div>
		<div class="form-group">
            <input type="password" class="form-control" name="password" placeholder="Senha" required="required">
        </div>   
    </form>     
        <div class="form-group">
            <button type="submit" class="btn btn-primary btn-lg btn-block">Fazer o login</button>
        </div>
    <p class="text-center small">Não tem uma conta? <a href="https://att1.websiteseguro.com/novoUser/index.php">Clique aqui para cadastrar!</a></p>
</div>
</body>
</html>

