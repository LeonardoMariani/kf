<!DOCTYPE html>
<html lang="pt-BR">
<head>
  <title>KF</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="../_css/bootstrap.min.css">  
  <script src="../_js/jquery.min.js"></script>
  <link href="../_css/estilo.css" rel="stylesheet">
  <script src="../_js/bootstrap.min.js"></script>
</head>
<body>
    
<div class="login-form">
    <div class="cadastro-div">
		<div class="avatar">
			<img src="../_imagens/iconSemBack.png" alt="Avatar">
		</div>
        <h2 class="text-center">Cadastro de usuário</h2>   
        <div class="form-group">
        	<input id="email" type="text" class="form-control" name="username" placeholder="Email" required="required">
        </div>
		<div class="form-group">
            <input id="senha1" type="password" class="form-control" name="senha1" placeholder="Senha" required="required">
        </div> 
		<div class="form-group">
            <input id="senha2" type="password" class="form-control" name="senha2" placeholder="Confirmar senha" required="required">
        </div>        
        <div class="form-group">
            <button onclick="proximo()" id="button" type="submit" class="btn btn-primary btn-lg btn-block">Próximo</button>
        </div>      
        <div id="senhasErradas" class="erroPass form-group">
            <p id="senhasErradas"><b>As senhas não coincidem. Tente novamente.</b></p>
        </div>
    </form>
</div>
<script>
    //Redireciona para a tela de selfie com o documento na mão.
    function proximo(){
        console.log("hey");
        var email = document.getElementById("email").value;
        var senha = document.getElementById("senha2").value;
        console.log(email);
        console.log(senha);
        localStorage.setItem("email", email);
        localStorage.setItem("senha", senha);
        document.location.href = "https://att1.websiteseguro.com/novoUser/selfie.php";

    }
    //Verifica se as senhas inseridas são as mesmas
    $(function(){
        $('#senha1, #senha2').on('keyup', function () {
            if ($('#senha1').val() == $('#senha2').val()) {
                $('#button').prop('disabled', false);
                $('#senhasErradas').css('display', 'none')
                //$('#senhasErradas').css('display', 'none')
            } else{
                $('#button').prop('disabled', true);
                //$('#senhasErradas').css('display', 'inline-block')
            } 
        });
        $("#senha2").on('keyup', function(){
            if ($('#senha1').val() == $('#senha2').val()) {
                $('#senhasErradas').css('display', 'none')
            } else{
                $('#senhasErradas').css('display', 'inline-block')
            } 
        });
    });

</script>
</body>
</html>

