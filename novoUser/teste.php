<!DOCTYPE html>
<html lang="pt-BR">
<head>
  <title>KF</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
		<!-- init -->
		<video id="video" width="640" height="480" autoplay></video>
		<button id="snap">Snap Photosss</button>
		<canvas id="canvas" width="640" height="480"></canvas>
		<!-- fim -->		
		<script>
		// Grab elements, create settings, etc.
			var video = document.getElementById('video');

			// Get access to the camera!
			if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
				// Not adding `{ audio: true }` since we only want video now
				navigator.mediaDevices.getUserMedia({ video: true }).then(function(stream) {
					//video.src = window.URL.createObjectURL(stream);
					video.srcObject = stream;
					video.play();
				});
			}
		// Elements for taking the snapshot
		var canvas = document.getElementById('canvas');
		var context = canvas.getContext('2d');
		var video = document.getElementById('video');

		// Trigger photo take
		document.getElementById("snap").addEventListener("click", function() {
			context.drawImage(video, 0, 0, 640, 480);
		});
		
		</script>
</body>
</html>

