<!DOCTYPE html>
<html lang="pt-BR">
<head>
  <title>KF</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="../_css/bootstrap.min.css">  
  <script src="../_js/jquery.min.js"></script>
  <link href="../_css/estilo.css" rel="stylesheet">
  <script src="../_js/bootstrap.min.js"></script>		
</head>
<body>

<div class="container">
		<div class="row">
				<div class="row-3 row-md-3 row-sm-3 row-lg-3">			
				<video id="video" playsinline autoplay></video>
			</div>
			<div class="row-3 row-md-3 row-sm-3 row-lg-3">				
				<button class="selfie btn-primary btn-lg btn-block" id="snap">Tirar foto do documento</button>
			</div>
			<div class="row-3 row-md-3 row-sm-3 row-lg-3">				
				<canvas id="canvasCapture" width="640" height="480"></canvas>					
			</div>
			<div class="row-3 row-md-3 row-sm-3 row-lg-3">									
				<button onclick="proximo()" class="selfie btn-primary btn-lg btn-block" id="send">Enviar foto</button>
			</div>
		</div>
</div>
<script>
//Ativar a gracação e habilitar o print de um frame dessa gravação no canvas
'use strict';
			const video = document.getElementById('video');
			const canvas = document.getElementById('canvasCapture');
			const snap = document.getElementById('snap');
			const send = document.getElementById('send');
			const errorMsgElement = document.getElementById('spanErrorMsg');
			
			const constraints = {
				video:{
					width: 640, height:480,
					facingMode: { 
						exact: 'environment'
						}
				}
			};
			
		async function init(){
			try{
				const stream = await navigator.mediaDevices.getUserMedia(constraints);
				handleSuccess(stream);
			}catch(e){
				errorMsgElement.innerHTML = `navigator.getUserMedia.error:${e.toString()}`			
			}
		}
		function handleSuccess(stream){
			window.stream = stream;
			video.srcObject = stream;
		}
		
		init();
		var context = canvas.getContext('2d');
		snap.addEventListener("click", function(){
			canvas.style.display = "inline-block";
			send.style.display = "inline-block";
			context.drawImage(video, 0, 0, 640, 480);
		});
// window.onload = function loadImg() { 
//     console.log(localStorage.getItem("email"));
//     console.log(localStorage.getItem("senha"));
//     var dataImage = localStorage.getItem('imgSelfie');    
//     const bannerImg = document.getElementById('tableBanner');
//     bannerImg.src = "data:image/png;base64," + dataImage;  
// } 

</script>	 
</body> 
</html>

